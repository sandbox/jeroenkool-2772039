#fb_page_block


##Description

fb_page_block created a block with the X ammount of (public) posts from a facebook page 
as a feed for your website. 

It depends on Bootstrap CSS classes to present the feed in a table.

## Installing fb_page_block
1. Install the module as normal
2. Configure the module at admin/structure/fb_page_block/settings
3. Feed the module with your long lived access token, page id and length of the feed

### tokens and page id's

Info on how to get your access token:
http://stackoverflow.com/questions/17197970/facebook-permanent-page-access-token

Info on how to get your page ID:
https://www.facebook.com/help/community/question/?id=378910098941520


##Credits:

Hacked together by J.Kool (jkool@integrative.it)
<<<<<<< HEAD

=======
>>>>>>> 0ffe38628a2cc9c433f944675812ace5a106ddd8
