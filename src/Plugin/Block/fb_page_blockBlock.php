<?php
/**
 * @file
 * Contains \Drupal\fb_page_block\Plugin\Block\fb_page_blockBlock.
 */
namespace Drupal\fb_page_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
/**
 * Provides a 'Facebook page' Block
 *
 * @Block(
 *   id = "fb_page_blockBlock",
 *   admin_label = @Translation("Facebook page Block"),
 *   category = @Translation("Blocks")
 * )
 */
class fb_page_blockBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
   
    #$config = $this->config(fb_page_block.settings);
    $accesstoken = \Drupal::config('fb_page_block.settings')->get('accesstoken');
    $pageid = \Drupal::config('fb_page_block.settings')->get('pageid');
    $feedlength = \Drupal::config('fb_page_block.settings')->get('feedlength');
    $statuses_url = 'https://graph.facebook.com/' . $pageid . '/posts?access_token=' . $accesstoken;
    $fetch_json   = file_get_contents($statuses_url);
    $return       = json_decode($fetch_json);
    $markup       = "<div class='table-responsive well well-sm'><table class='table'><tbody>";
    $i = 0;
    foreach ($return->data as $line){
        $i=$i+1;
        $postlink = "https://www.facebook.com/" . $line->id;
        $markup = $markup . "<tr>";
        $markup = $markup . "<td><img src='" . $line->picture . "' alt = '" .$line->name ."' </img></td>";
        $markup = $markup . "<td><a href= '" . $postlink . "' target = '_blank'><h2> " . $line->name . "</h2></a><br>". $line->message ."</td>";
        $markup = $markup . "</tr>";
        if ($i > $feedlength) {break;}
    } 
    $markup = $markup . "<tbody></table></div>";
    return array(
      '#markup' => $this->t($markup),
    );
  }

}
?>

