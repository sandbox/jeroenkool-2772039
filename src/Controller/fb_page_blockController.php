<?php

/**
 * @file
 * Contains \Drupal\controller\fb_page_blockController.
 */

namespace Drupal\fb_page_block\Controller;

use Drupal\Core\Controller\ControllerBase;

class fb_page_blockController extends ControllerBase {
  public function content() {

    return array(
        '#type' => 'markup',
        '#markup' => $this->t("test"),
    );
  }
}
?>
