<?php
namespace Drupal\fb_page_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure fb_page_block settings for this site.
 */
class fb_page_blockSettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fb_page_blockSettings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'accesstoken','pageid','feedlength',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fb_page_block.settings');
    $form['fb_page_block_accesstoken'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Facebook page: access token'),
      '#default_value' => $config->get('accesstoken'),
    );
    $form['fb_page_block_pageid'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Facebook page: id'),
      '#default_value' => $config->get('pageid'),
    );
    $form['fb_page_block_feedlength'] = array(
      '#type' => 'number',
      '#title' => $this->t('Feed length'),
      '#default_value' => $config->get('feedlength'),
    );

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    #$config = $this->config('fb_page_block.settings');
    $config = \Drupal::configFactory()->getEditable('fb_page_block.settings');
    $config
      ->set('accesstoken', $form_state->getValue('fb_page_block_accesstoken'))
      ->set('pageid', $form_state->getValue('fb_page_block_pageid'))
      ->set('feedlength', $form_state->getValue('fb_page_block_feedlength'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
?>

